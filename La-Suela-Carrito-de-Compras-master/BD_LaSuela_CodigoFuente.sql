USE [master]
GO
/****** Object:  Database [Restaurante]    Script Date: 14/01/2019 17:03:15 ******/
CREATE DATABASE [restaurante]

ALTER DATABASE [Restaurante] SET COMPATIBILITY_LEVEL = 120
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [Restaurante].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [Zapaeria] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [Zapaeria] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [Zapaeria] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [Zapaeria] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [Zapaeria] SET ARITHABORT OFF 
GO
ALTER DATABASE [Zapaeria] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [Zapaeria] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [Zapaeria] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [Zapaeria] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [Zapaeria] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [Zapaeria] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [Zapaeria] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [Zapaeria] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [Zapaeria] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [Zapaeria] SET  DISABLE_BROKER 
GO
ALTER DATABASE [Zapaeria] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [Zapaeria] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [Zapaeria] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [Zapaeria] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [Zapaeria] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [Zapaeria] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [Zapaeria] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [Zapaeria] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [Zapaeria] SET  MULTI_USER 
GO
ALTER DATABASE [Zapaeria] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [Zapaeria] SET DB_CHAINING OFF 
GO
ALTER DATABASE [Zapaeria] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [Zapaeria] SET TARGET_RECOVERY_TIME = 0 SECONDS 
GO
ALTER DATABASE [Zapaeria] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [Zapaeria] SET QUERY_STORE = OFF
GO
USE [Zapaeria] 
GO
/****** Object:  Table [dbo].[DBS]    Script Date: 14/01/2019 17:03:16 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DBS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdPedido] [int] NULL,
	[MontoTotal] [decimal](18, 0) NULL,
	[IdModoDePago] [int] NULL,
 CONSTRAINT [PK_DBS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DetalleBoleta]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DetalleBoleta](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdPedido] [int] NOT NULL,
	[MontoTotal] [decimal](18, 2) NOT NULL,
	[IdModoDePago] [int] NOT NULL,
 CONSTRAINT [PK_DetalleBoleta] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Empleado]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Empleado](
	[ID] [nvarchar](50) NOT NULL,
	[Contraseña] [nvarchar](50) NOT NULL,
	[IdTipoEmpleado] [int] NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[Celular] [int] NOT NULL,
	[Correo] [nvarchar](100) NOT NULL,
	[Direccion] [nvarchar](50) NOT NULL,
	[Sueldo] [money] NOT NULL,
 CONSTRAINT [PK_Empleado] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ModoDePago]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ModoDePago](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_ModoDePago] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Pedido]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Pedido](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Atendido] [bit] NOT NULL,
	[Detalle] [nvarchar](50) NOT NULL,
	[IdEmpleado] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_Pedido] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PPS]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PPS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdProducto] [int] NULL,
	[IdPedido] [int] NULL,
	[Observacion] [nvarchar](50) NULL,
 CONSTRAINT [PK_PPS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Producto]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Producto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [nvarchar](50) NOT NULL,
	[IdTipoProducto] [int] NOT NULL,
	[Precio] [decimal](18, 2) NOT NULL,
	[Descripcion] [nvarchar](100) NOT NULL,
	[imagen] [nvarchar](50) NULL,
 CONSTRAINT [PK_Producto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[Productos_Pedidos]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Productos_Pedidos](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[IdProducto] [int] NOT NULL,
	[IdPedido] [int] NOT NULL,
	[Observacion] [nvarchar](50) NULL,
 CONSTRAINT [PK_Productos_Pedidos] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[PS]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[PS](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Atendido] [bit] NULL,
	[Detalle] [nvarchar](50) NULL,
 CONSTRAINT [PK_PS] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoEmpleado]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoEmpleado](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
	[Sueldo] [decimal](18, 2) NULL,
 CONSTRAINT [PK_TipoEmpleado] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TipoProducto]    Script Date: 14/01/2019 17:03:17 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TipoProducto](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[Descripcion] [nvarchar](50) NOT NULL,
 CONSTRAINT [PK_TipoProducto] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[DetalleBoleta] ON 

INSERT [dbo].[DetalleBoleta] ([ID], [IdPedido], [MontoTotal], [IdModoDePago]) VALUES (1, 1, CAST(30.30 AS Decimal(18, 2)), 1)
INSERT [dbo].[DetalleBoleta] ([ID], [IdPedido], [MontoTotal], [IdModoDePago]) VALUES (2, 2, CAST(36.50 AS Decimal(18, 2)), 1)
SET IDENTITY_INSERT [dbo].[DetalleBoleta] OFF
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'ADMINMax', N'1234', 1, N'Max', 998877665, N'max2.lara435676@gmail.com', N'Alipio Ponce', 1200.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'ADMINRenato', N'1234', 1, N'Renato Zegarra Villa', 944645999, N'rezv98@gmail.com', N'Av Tupac 191', 0.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'ADMINRodrigo', N'1234', 1, N'Rodrigo Max Lara Camarena', 988403355, N'max.lara@hotmail.com', N'Av. LosAlamos 141', 0.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'ALC', N'1234', 5, N'Andrea Lara Camarena', 944645263, N'andrealara@hotmail.com', N'Av. LosAlamos 141', 0.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'CAAndrea', N'12345678', 2, N'Andrea', 988403360, N'andrea@hotmail.com', N'Av. Los Alamos 141', 800.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'CARodrigo', N'123456', 2, N'Rodrigo', 944645888, N'max.lara99999999@gmail.com', N'Av. Mariscal La Mar 175', 800.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'CodigoFuente', N'1234', 5, N'Rodrigo Max Lara Camarena', 944645247, N'CodigoFuente@gmail.com', N'Av. Mariscal La Mar 673', 0.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'COJorge', N'abcd', 3, N'Jorge', 999999999, N'jorge2@gmail.com', N'calle alamos 260', 600.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'CORenato', N'renato777', 3, N'Renato', 984018241, N'rezv98@gmail.com', N'Av Los Incas 323', 600.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'COSergio', N'12345', 3, N'Sergio', 998877664, N'sergio.slc@hotmail.com', N'Resid.Inclan Block 20', 600.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'daniprofe', N'1234', 5, N'daniel', 123456789, N'dancarvas2431@gmail.com', N'calle upc', 0.0000)
INSERT [dbo].[Empleado] ([ID], [Contraseña], [IdTipoEmpleado], [Nombre], [Celular], [Correo], [Direccion], [Sueldo]) VALUES (N'nuevo', N'1234', 2, N'pepe lucho di maria', 456789032, N'asd@upc.pe', N'av. la lucha', 0.0000)
SET IDENTITY_INSERT [dbo].[ModoDePago] ON 

INSERT [dbo].[ModoDePago] ([ID], [Descripcion]) VALUES (1, N'efectivo')
INSERT [dbo].[ModoDePago] ([ID], [Descripcion]) VALUES (2, N'visa')
INSERT [dbo].[ModoDePago] ([ID], [Descripcion]) VALUES (3, N'mastercard')
INSERT [dbo].[ModoDePago] ([ID], [Descripcion]) VALUES (4, N'dolares')
SET IDENTITY_INSERT [dbo].[ModoDePago] OFF
SET IDENTITY_INSERT [dbo].[Pedido] ON 

INSERT [dbo].[Pedido] ([ID], [Atendido], [Detalle], [IdEmpleado]) VALUES (1, 0, N'Andrea Lara Camarena', N'ADMINMax')
INSERT [dbo].[Pedido] ([ID], [Atendido], [Detalle], [IdEmpleado]) VALUES (2, 0, N'Rodrigo Max Lara Camarena', N'ADMINMax')
SET IDENTITY_INSERT [dbo].[Pedido] OFF
SET IDENTITY_INSERT [dbo].[Producto] ON 

INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (1, N'ADIDAS1', 1, CAST(550.00 AS Decimal(18, 2)), N'ADIDAS FORUM HOMBRE', N'ADIDAS1.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (2, N'ADIDAS2', 1, CAST(675.00 AS Decimal(18, 2)), N'ADIDAS NMD HOMBRE', N'ADIDAS2.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (3, N'ADIDAS3', 1, CAST(450.00 AS Decimal(18, 2)), N'ADIDAS TRX VINTAGE HOMBRE', N'ADIDAS3.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (4, N'ADIDAS4', 1, CAST(800.00 AS Decimal(18, 2)), N'ADIDAS ZX 1K BOST HOMBRE', N'ADIDAS4.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (5, N'ADIDAS5', 1, CAST(725.00 AS Decimal(18, 2)), N'ADIDAS ULTRABOOST HOMBRE', N'ADIDAS5.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (6, N'ADIDAS6', 1, CAST(350.00 AS Decimal(18, 2)), N'ADIDAS SWIFT HOMBRE', N'ADIDAS6.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (7, N'ADIDAS7', 1, CAST(580.00 AS Decimal(18, 2)), N'ADIDAS TEN REBOUND', N'ADIDAS7.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (8, N'CONVERSE', 1, CAST(335.00 AS Decimal(18, 2)), N'CONVERSE ALL STAR MUJER', N'CONVERSE.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (9, N'NIKE1', 1, CAST(650.00 AS Decimal(18, 2)), N'NIKE REACT INFINITY HOMBRE', N'NIKE1.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (10, N'NIKE2', 1, CAST(700.00 AS Decimal(18, 2)), N'NIKE FORCE 1 HOMBRE', N'NIKE2.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (11, N'NIKE3', 2, CAST(695.95 AS Decimal(18, 2)), N'NIKE W VISTA LITE MUJER', N'NIKE3.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (12, N'NIKE4', 2, CAST(585.50 AS Decimal(18, 2)), N'NIKE WMNS AIR FORCE HOMBRE', N'NIKE4.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (13, N'NIKE5', 2, CAST(695.50 AS Decimal(18, 2)), N'NIKE AIR MAX 97 MUJER', N'NIKE5.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (14, N'NIKE6', 2, CAST(499.99 AS Decimal(18, 2)), N'NIKE AIR HUARACHE HOMBRE', N'NIKE6.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (15, N'NIKE7', 2, CAST(350.25 AS Decimal(18, 2)), N'NIKE JORDAN MUJER', N'NIKE7.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (16, N'NIKE8', 2, CAST(450.25 AS Decimal(18, 2)), N'NIKE CHRON 2 HOMBRE', N'NIKE8.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (17, N'PUMA1', 2, CAST(775.00 AS Decimal(18, 2)), N'PUMA SUEDE CLASSIC HOMBRE', N'PUMA1.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (18, N'PUMA2', 2, CAST(800.00 AS Decimal(18, 2)), N'PUMA RIDER PLAY MUJER', N'PUMA2.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (19, N'PUMA3', 2, CAST(900.00 AS Decimal(18, 2)), N'PUMA ROMA BASIC HOMBRE', N'PUMA3.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (20, N'PUMA4', 3, CAST(300.00 AS Decimal(18, 2)), N'PUMA SPARCO SPEED HOMBRE', N'PUMA4.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (21, N'PUMA5', 3, CAST(200.00 AS Decimal(18, 2)), N'PUMA BARI MID MUJER', N'PUMA5.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (22, N'PUMA6', 3, CAST(275.00 AS Decimal(18, 2)), N'PUMA RIDER PLAY ON W MUJER', N'PUMA6.png')
INSERT [dbo].[Producto] ([ID], [Nombre], [IdTipoProducto], [Precio], [Descripcion], [imagen]) VALUES (23, N'VANS', 3, CAST(600.00 AS Decimal(18, 2)), N'VANS CHEAP MUJER', N'VANS.png')

SET IDENTITY_INSERT [dbo].[Producto] OFF
SET IDENTITY_INSERT [dbo].[Productos_Pedidos] ON 

INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (1, 1, 1, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (2, 7, 1, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (3, 16, 1, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (4, 1, 2, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (5, 4, 2, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (6, 11, 2, NULL)
INSERT [dbo].[Productos_Pedidos] ([ID], [IdProducto], [IdPedido], [Observacion]) VALUES (7, 19, 2, NULL)
SET IDENTITY_INSERT [dbo].[Productos_Pedidos] OFF
SET IDENTITY_INSERT [dbo].[TipoEmpleado] ON 

INSERT [dbo].[TipoEmpleado] ([ID], [Descripcion], [Sueldo]) VALUES (1, N'admin', CAST(1200.00 AS Decimal(18, 2)))
INSERT [dbo].[TipoEmpleado] ([ID], [Descripcion], [Sueldo]) VALUES (2, N'cajero', CAST(800.00 AS Decimal(18, 2)))
INSERT [dbo].[TipoEmpleado] ([ID], [Descripcion], [Sueldo]) VALUES (3, N'cocinero', CAST(600.00 AS Decimal(18, 2)))
INSERT [dbo].[TipoEmpleado] ([ID], [Descripcion], [Sueldo]) VALUES (5, N'cliente', CAST(0.00 AS Decimal(18, 2)))
INSERT [dbo].[TipoEmpleado] ([ID], [Descripcion], [Sueldo]) VALUES (6, N'Seguridad', CAST(900.00 AS Decimal(18, 2)))
SET IDENTITY_INSERT [dbo].[TipoEmpleado] OFF
SET IDENTITY_INSERT [dbo].[TipoProducto] ON 

INSERT [dbo].[TipoProducto] ([ID], [Descripcion]) VALUES (1, N'hamburguesa')
INSERT [dbo].[TipoProducto] ([ID], [Descripcion]) VALUES (2, N'complemento')
INSERT [dbo].[TipoProducto] ([ID], [Descripcion]) VALUES (3, N'combo')
SET IDENTITY_INSERT [dbo].[TipoProducto] OFF
ALTER TABLE [dbo].[DBS]  WITH CHECK ADD  CONSTRAINT [FK_DBS_ModoDePago] FOREIGN KEY([IdModoDePago])
REFERENCES [dbo].[ModoDePago] ([ID])
GO
ALTER TABLE [dbo].[DBS] CHECK CONSTRAINT [FK_DBS_ModoDePago]
GO
ALTER TABLE [dbo].[DBS]  WITH CHECK ADD  CONSTRAINT [FK_DBS_PS] FOREIGN KEY([IdPedido])
REFERENCES [dbo].[PS] ([ID])
GO
ALTER TABLE [dbo].[DBS] CHECK CONSTRAINT [FK_DBS_PS]
GO
ALTER TABLE [dbo].[DetalleBoleta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleBoleta_ModoDePago] FOREIGN KEY([IdModoDePago])
REFERENCES [dbo].[ModoDePago] ([ID])
GO
ALTER TABLE [dbo].[DetalleBoleta] CHECK CONSTRAINT [FK_DetalleBoleta_ModoDePago]
GO
ALTER TABLE [dbo].[DetalleBoleta]  WITH CHECK ADD  CONSTRAINT [FK_DetalleBoleta_Pedido] FOREIGN KEY([IdPedido])
REFERENCES [dbo].[Pedido] ([ID])
GO
ALTER TABLE [dbo].[DetalleBoleta] CHECK CONSTRAINT [FK_DetalleBoleta_Pedido]
GO
ALTER TABLE [dbo].[Empleado]  WITH CHECK ADD  CONSTRAINT [FK_Empleado_TipoEmpleado] FOREIGN KEY([IdTipoEmpleado])
REFERENCES [dbo].[TipoEmpleado] ([ID])
GO
ALTER TABLE [dbo].[Empleado] CHECK CONSTRAINT [FK_Empleado_TipoEmpleado]
GO
ALTER TABLE [dbo].[Pedido]  WITH CHECK ADD  CONSTRAINT [FK_Pedido_Pedido] FOREIGN KEY([IdEmpleado])
REFERENCES [dbo].[Empleado] ([ID])
GO
ALTER TABLE [dbo].[Pedido] CHECK CONSTRAINT [FK_Pedido_Pedido]
GO
ALTER TABLE [dbo].[PPS]  WITH CHECK ADD  CONSTRAINT [FK_PPS_Producto] FOREIGN KEY([IdProducto])
REFERENCES [dbo].[Producto] ([ID])
GO
ALTER TABLE [dbo].[PPS] CHECK CONSTRAINT [FK_PPS_Producto]
GO
ALTER TABLE [dbo].[PPS]  WITH CHECK ADD  CONSTRAINT [FK_PPS_PS] FOREIGN KEY([IdPedido])
REFERENCES [dbo].[PS] ([ID])
GO
ALTER TABLE [dbo].[PPS] CHECK CONSTRAINT [FK_PPS_PS]
GO
ALTER TABLE [dbo].[Producto]  WITH CHECK ADD  CONSTRAINT [FK_Producto_TipoProducto] FOREIGN KEY([IdTipoProducto])
REFERENCES [dbo].[TipoProducto] ([ID])
GO
ALTER TABLE [dbo].[Producto] CHECK CONSTRAINT [FK_Producto_TipoProducto]
GO
ALTER TABLE [dbo].[Productos_Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Pedidos_Pedido] FOREIGN KEY([IdPedido])
REFERENCES [dbo].[Pedido] ([ID])
GO
ALTER TABLE [dbo].[Productos_Pedidos] CHECK CONSTRAINT [FK_Productos_Pedidos_Pedido]
GO
ALTER TABLE [dbo].[Productos_Pedidos]  WITH CHECK ADD  CONSTRAINT [FK_Productos_Pedidos_Producto] FOREIGN KEY([IdProducto])
REFERENCES [dbo].[Producto] ([ID])
GO
ALTER TABLE [dbo].[Productos_Pedidos] CHECK CONSTRAINT [FK_Productos_Pedidos_Producto]
GO
/****** Object:  StoredProcedure [dbo].[eliminar]    Script Date: 14/01/2019 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[eliminar]
AS
BEGIN
	delete from DBS 
	delete from PPS
    delete from PS
	DBCC CHECKIDENT (PS, RESEED,0)
	DBCC CHECKIDENT (PPS, RESEED,0)
	DBCC CHECKIDENT (DBS, RESEED,0)
END
GO
/****** Object:  StoredProcedure [dbo].[eliminarTodo]    Script Date: 14/01/2019 17:03:18 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,,Name>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE [dbo].[eliminarTodo] 
	-- Add the parameters for the stored procedure here
AS
BEGIN
delete from DBS 
	delete from PPS
    delete from PS
	DBCC CHECKIDENT (PS, RESEED,0)
	DBCC CHECKIDENT (PPS, RESEED,0)
	DBCC CHECKIDENT (DBS, RESEED,0)
	END
GO
USE [master]
GO
ALTER DATABASE [Restaurante] SET  READ_WRITE 
GO
